import io
from django.utils.decorators import method_decorator
from django.views import View
from .serialization import StudentSerializer
from .models import StudentModel
from django.views.decorators.csrf import csrf_exempt
from rest_framework.parsers import JSONParser
from django.http import JsonResponse


# Create your views here.
stu = StudentModel.objects.all()
serial = StudentSerializer(stu, many=True)
li1 = []
for i in serial.data:
    li1.append(i['id'])
li_keys = list(serial.data[0].keys())


@method_decorator(csrf_exempt, name='dispatch')
# This is for Post the data
class StudentViewPost(View):

    def post(self, request):
        data = request.body    # extract the data from request body
        if data == b'':    # Checked that if request is empty or not
            return JsonResponse({'msg': 'Please provide the data to be inserted'})    # If empty then
        else:
            # if request contains data then
            stream = io.BytesIO(data)    # Convert the data into bytes
            python_data = JSONParser().parse(stream)    # Convert the data into python data types
        serial_post = StudentSerializer(data=python_data)    # This is for Validation purpose
        if serial_post.is_valid():    # For checking the validity of the given data as per schema/model
            serial_post.save()    # If data schema is valid then save it to the Model/Database
            return JsonResponse({'msg': 'Successfull', 'Inserted_name': serial_post.data.get('name')}, safe=False)    # Return Jsonresponse
        return JsonResponse(serial_post.errors)    # If code is not valid then it throw the error


# This is for Get/read the data
class StudentViewGet(View):

    def get(self, request):
        data = request.body
        if data == b'':    # Checked that if request is empty or not
            python_data = {'id': None}    # If empty then
        else:
            # if request contains data then
            stream = io.BytesIO(data)
            python_data = JSONParser().parse(stream)
        if 'id' in python_data.keys():    # if id is in python data then
            id_ = python_data.get('id')    # extract the id from python data
            if id_ is not None:    # Checked if id is None then
                if id_ in li1:    # Check that if id is in the database
                    stu_get = StudentModel.objects.get(id=id_)    # extract the data based on id from the Student Model
                    serial_get = StudentSerializer(stu_get)    # make the serializer based on the model
                    return JsonResponse(serial_get.data)    # return the Jsonresponse containing the data
                else:
                    return JsonResponse({'msg': 'Please write valid key', 'valid_ids': li1})    # if id not in database then return this message
            else:
                stu_get = StudentModel.objects.all()    # If id is None means we have to extract the all objects from the models
                serial_get = StudentSerializer(stu_get, many=True)    # make the serializer based on the objects from stu_get
                return JsonResponse(serial_get.data, safe=False)    # return Jsonresponse contains the data
        else:
            return JsonResponse({'msg': 'Please give the id to get the values'})    # if id fields is not in python data return the JsonResponse


@method_decorator(csrf_exempt, name='dispatch')
# This is for Put/Update the data
class StudentViewPut(View):

    def put(self, request):
        data = request.body
        if data == b'':    # Checked that if request is empty or not
            return JsonResponse({'msg': 'Please provide the data to be inserted'})
        stream = io.BytesIO(data)
        python_data = JSONParser().parse(stream)
        li = list(python_data.keys())    # extract all the keys from python data as list
        for j in li:
            if j not in li_keys:
                return JsonResponse({'msg': 'Please provide valid field names', 'Fields': li_keys})    # return this Jsonresponse if keys is not as defined.
        if 'id' in python_data.keys():
            id_ = python_data.get('id')
            if id_ not in li1:
                return JsonResponse({'msg': 'Please choose Valid ids',
                                     'Valid_ids': li1})
            stu_put = StudentModel.objects.get(id=id_)
            serial_original_put = StudentSerializer(stu_put).data
            serial_update = StudentSerializer(stu_put, data=python_data, partial=True)
            if serial_update.is_valid():
                serial_update.save()
                stu_put = StudentModel.objects.get(id=id_)
                serial_update = StudentSerializer(stu_put)
                return JsonResponse({'msg': 'Successfull Update',
                                       'Previous_data': serial_original_put,
                                       'Updated_data': serial_update.data}, safe=False)
            return JsonResponse(serial_update.errors)
        else:
            return JsonResponse({'msg': 'I want id to update',
                                     'Valid_ids': li1})


@method_decorator(csrf_exempt, name='dispatch')
class StudentViewDelete(View):

    def delete(self, request):
        data = request.body
        if data == b'':    # Checked that if request is empty or not
            return JsonResponse({'msg': 'Please provide the data to be inserted'})
        stream = io.BytesIO(data)
        python_data = JSONParser().parse(stream)
        li = python_data.keys()
        if 'id' not in li:
            return JsonResponse({'msg': 'You have to give me ID'})
        id_ = python_data.get('id')
        if (id_ in li1) or (int(id_) in li1):
            stu_delete = StudentModel.objects.get(id=id_)
            deleted_data = StudentSerializer(stu_delete).data
            stu_delete.delete()
            return JsonResponse({'msg': 'Data Delete',
                                 'Deleted Data': deleted_data})
        return JsonResponse({'Error': 'You have to give me a valid ID',
                             'ID_Present': li1}, safe=False)
